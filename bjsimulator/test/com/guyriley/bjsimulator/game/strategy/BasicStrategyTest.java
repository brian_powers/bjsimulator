/**
 * 
 */
package com.guyriley.bjsimulator.game.strategy;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.guyriley.bjsimulator.game.Hand;
import com.guyriley.bjsimulator.game.Shoe;
import com.guyriley.bjsimulator.game.strategy.Strategy.Decision;

/**
 * @author Brian S. Powers
 *
 */
public class BasicStrategyTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for {@link com.guyriley.bjsimulator.game.strategy.BasicStrategy#getDecision(com.guyriley.bjsimulator.game.Hand, com.guyriley.bjsimulator.game.rules.Rules)}.
	 */
	@Test
	public void testGetDecision() {
		Strategy bs = new BasicStrategy();
		assertEquals(bs.getDecision(null, null, null), Decision.STAND);
		Math.abs(0);
	}
	
	@Test
	public void testManyDecisions() {
		Shoe shoe = new Shoe(1000);
		Strategy strategy = new BasicStrategy();
		
		int handsPlayed = 0;
		while (shoe.getRemainingPercent() > 10) {
			Hand dealerHand = new Hand();
			Hand playerHand = new Hand();

			dealerHand.addCard(shoe.dealCard());
			playerHand.addCard(shoe.dealCard());
			dealerHand.addCard(shoe.dealCard());
			playerHand.addCard(shoe.dealCard());
			
			System.out.print(String.format("%03d", shoe.getRemainingPercent()) + "% ");
			System.out.print(playerHand.getCard(0).getRank().toString() + ", ");
			System.out.print(playerHand.getCard(1).getRank().toString() + ": ");
			System.out.print(playerHand.getValue() + " / ");
			System.out.print("Dealer: " + dealerHand.getCard(1).getRank().toString() + " / ");
			System.out.println("Decision: " + strategy.getDecision(playerHand, dealerHand.getCard(1).getRank(), shoe));
			handsPlayed++;
		}
		
		System.out.println("\nHands Played: " + handsPlayed);

	}

}
