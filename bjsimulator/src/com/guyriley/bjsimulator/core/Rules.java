/**
 * 
 */
package com.guyriley.bjsimulator.core;

/**
 * @author Brian S. Powers
 *
 */
public final class Rules {

	static final boolean DOUBLE_AFTER_SPLIT = true;
	static final boolean SURRENDER = true;
	static final boolean SPLIT_ACES_NUM = true;
}
