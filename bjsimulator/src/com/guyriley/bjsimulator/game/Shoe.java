/**
 * BJSimulator
 * Copyright 2010 Brian S. Powers (brian.s.powers@gmail.com)
 */
package com.guyriley.bjsimulator.game;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Deck
 * @author Brian S. Powers
 */
public class Shoe {

	private List<Card> activeCards;
	private List<Card> inactiveCards;
	private int decks;
	
	/**
	 * Default constructor, creates a Deck with 52 Cards.
	 */
	public Shoe() {
		this(1);
	}

	/**
	 * 
	 * @param numdecks
	 */
	public Shoe(int numdecks) {
		decks = numdecks;
		activeCards = new ArrayList<Card>();
		inactiveCards = new ArrayList<Card>();

		for (int k = 1; k <= decks; k++) {
			for (Card.Suit suit : Card.Suit.values()) {
				for (Card.Rank rank : Card.Rank.values()) {
					activeCards.add(new Card(suit,rank));
				}
			}
		}

		shuffle();
	}

	public Card dealCard() {
		Card returncard = activeCards.get(0);
		activeCards.remove(0);
		inactiveCards.add(returncard);
		return returncard;
	}

	public Boolean hasCards() {
		if (activeCards.size() > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public void shuffle() {
		// Shift all inactive cards to active
		for (Card currentCard : inactiveCards) {
			activeCards.add(currentCard);
			inactiveCards.remove(currentCard);
		}
		Collections.shuffle(activeCards);
	}
	
	public int getRemaining() {
		return activeCards.size();
	}
	
	public int getRemainingPercent() {
		return (int)((float)activeCards.size() / (float)(decks * 52) * 100);
	}
}
