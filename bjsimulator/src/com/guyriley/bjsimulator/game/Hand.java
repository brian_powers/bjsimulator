/**
 * 
 */
package com.guyriley.bjsimulator.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Brian S. Powers
 * 
 */
public class Hand {

	private List<Card> cards;
	private boolean hasAce;
	private boolean wasSplit;
	private boolean splitAces;
	private boolean isSoft;
	private int value;
	private boolean canSplit;
	private boolean canDouble;

	/**
	 * 
	 */
	public Hand() {
		this(false,false);
	}

	public Hand(boolean wasSplit, boolean splitAces) {
		cards = new ArrayList<Card>();
		hasAce = isSoft = false;
		this.wasSplit = wasSplit;
		this.splitAces = splitAces;
	}

	/**
	 * Adds one Card to the Hand
	 * 
	 * @param newcard
	 */
	public void addCard(Card newcard) {
		cards.add(newcard);
		if (newcard.getValue() == 1) {
			hasAce = true;
		}

		calculateValue();
	}

	/**
	 * Gets the card at the specified index.
	 * 
	 * @param index
	 * @return Card
	 */
	public Card getCard(int index) {
		return cards.get(index);
	}
	/**
	 * Calculate hand value considering Ace as 11 if possible
	 */
	private void calculateValue() {
		value = 0;
		for (Card currentCard : cards) {
			value += currentCard.getValue();
		}

		// If an Ace is in the hand, see if we can play it as an 11
		isSoft = false;
		if (hasAce == true) {
			if (value + 10 <= 21) {
				value += 10;
				isSoft = true;
			}
		}

		// Check to see if we can double down & split
		// TODO: Consider house rules
		canDouble = false;
		canSplit = false;
		if (cards.size() == 2) {
			canDouble = true;
			if (cards.get(0).getValue() == cards.get(1).getValue()) {
				canSplit = true;
			}
		}
	}

	/**
	 * Check to see if Doubling is allowed
	 * 
	 * @return boolean
	 */
	public boolean canDouble() {
		return canDouble;
	}

	/**
	 * Check to see if Splitting is allowed
	 * 
	 * @return boolean
	 */
	public boolean canSplit() {
		return canSplit;
	}

	/**
	 * getCardCount() - Get count of cards in Hand
	 * 
	 * @return int
	 */
	public int getCardCount()
	{
		return cards.size();
	}
	
	/**
	 * Return BJ Hand value
	 * 
	 * @return int value
	 */
	public int getValue() {
		return value;
	}

	public boolean isSoft() {
		return isSoft;
	}
	
	public boolean isPair() {
		if (cards.size() == 2) {
			if (cards.get(0).getValue() == cards.get(1).getValue()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @return boolean wasSplit
	 */
	public boolean wasSplit() {
		return wasSplit;
	}

	/**
	 * @return boolean splitAces
	 */
	public boolean splitAces() {
		return splitAces;
	}

}
