/**
 * 
 */
package com.guyriley.bjsimulator.game;

/**
 * @author Brian S. Powers
 *
 */
public class Card {

	private Suit suit;
	private Rank rank;

	public enum Rank {
		TWO   (2, "Two"),
		THREE (3, "Three"),
		FOUR  (4, "Four"),
		FIVE  (5, "Five"),
		SIX   (6, "Six"),
		SEVEN (7, "Seven"),
		EIGHT (8, "Eight"),
		NINE  (9, "Nine"),
		TEN   (10,"Ten"),
		JACK  (10,"Jack"),
		QUEEN (10,"Queen"),
		KING  (10,"King"),
		ACE   (1, "Ace");
		
		private final int value;
		private final String name;
		
		Rank(int value, String name) {
			this.value = value;
			this.name = name;
		}
		
		public int value() {
			return value;
		}
		
		public String toString() {
			return name;
		}
	}
	
	public enum Suit {
		SPADES ("Spades"),
		HEARTS ("Hearts"),
		DIAMONDS ("Diamonds"),
		CLUBS ("Clubs");
		
		private final String name;
		
		Suit(String name) {
			this.name = name;
		}
		
		public String toString() {
			return name;
		}
	}
	
	/**
	 * @param suit
	 * @param rank
	 */
	public Card(Suit suit, Rank rank) {

		this.suit = suit;
		this.rank = rank;
		
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return rank.value();
	}

	/**
	 * @return the suit
	 */
	public Suit getSuit() {
		return suit;
	}

	/**
	 * @param suit the suit to set
	 */
	public void setSuit(Suit suit) {
		this.suit = suit;
	}

	/**
	 * @return the rank
	 */
	public Rank getRank() {
		return rank;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (rank.toString() + " of " + suit.toString());
	}

}
