/**
 * 
 */
package com.guyriley.bjsimulator.game.strategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.guyriley.bjsimulator.game.Card.Rank;
import com.guyriley.bjsimulator.game.Hand;
import com.guyriley.bjsimulator.game.Shoe;

/**
 * @author Brian S. Powers
 *
 */
public class BasicStrategy implements Strategy {

	//Map<HandTotal,Map<DealerUpCardValue,DecisionString>>
	Map<Integer,Map<Integer,String>> pairScenarios = new HashMap<Integer,Map<Integer,String>>();
	Map<Integer,Map<Integer,String>> softScenarios = new HashMap<Integer,Map<Integer,String>>();
	Map<Integer,Map<Integer,String>> hardScenarios = new HashMap<Integer,Map<Integer,String>>();
	
	public BasicStrategy() {

		// Read in basic strategy from specially formatted & located CSV file.
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/com/guyriley/bjsimulator/resources/basic.csv")));

			String line;
			while ((line = in.readLine()) != null) {
				String[] tokens = line.split(",");
				if (tokens[0].trim().equals("PAIR")) {
					Map<Integer,String> innerMap = new HashMap<Integer,String>();
					for (int i=2; i <= 11; i++) {
						innerMap.put(i, tokens[i].trim());
					}
					pairScenarios.put(Integer.parseInt(tokens[1]), innerMap);
				}
				if (tokens[0].trim().equals("SOFT")) {
					Map<Integer,String> innerMap = new HashMap<Integer,String>();
					for (int i=2; i <= 11; i++) {
						innerMap.put(i, tokens[i].trim());
					}
					softScenarios.put(Integer.parseInt(tokens[1]), innerMap);
				}
				if (tokens[0].trim().equals("HARD")) {
					Map<Integer,String> innerMap = new HashMap<Integer,String>();
					for (int i=2; i <= 11; i++) {
						innerMap.put(i, tokens[i].trim());
					}
					hardScenarios.put(Integer.parseInt(tokens[1]), innerMap);
				}
			}

		} catch (IOException e) {
			throw(new RuntimeException("Error loading basic strategy.  Could not read strategy file."));
		} catch (NullPointerException e) {
			throw(new RuntimeException("Error loading basic strategy.  Bad strategy file format."));
		}

	}
	
	
	/* (non-Javadoc)
	 * @see com.guyriley.bjsimulator.game.strategy.Strategy#getDecision(com.guyriley.bjsimulator.game.Hand, com.guyriley.bjsimulator.game.rules.Rules)
	 */
	@Override
	public Decision getDecision(Hand hand, Rank upCardRank, Shoe shoe) {
		
		int dealerValue = upCardRank.value() == 1 ? 11 : upCardRank.value();
		
		if (hand.getValue() == 21) {
			return Decision.STAND;
		} 

		if (hand.isPair() && hand.canSplit()) {
			return translateDecision(pairScenarios.get(hand.getValue()).get(dealerValue), hand);
		} else if (hand.isSoft()) {
			return translateDecision(softScenarios.get(hand.getValue()).get(dealerValue), hand);
		} else {
			return translateDecision(hardScenarios.get(hand.getValue()).get(dealerValue), hand);
		}
	}

	Decision translateDecision(String directive, Hand hand) {
		if (directive.equalsIgnoreCase("H")) {
			return Decision.HIT;
		} 
		else if (directive.equalsIgnoreCase("SP")) {
			return Decision.SPLIT;
		}
		else if (directive.equalsIgnoreCase("Dh")) {
			if (hand.canDouble()) {
				return Decision.DOUBLE;
			} else {
				return Decision.HIT;
			}
		}
		else if (directive.equalsIgnoreCase("Ds")) {
			if (hand.canDouble()) {
				return Decision.DOUBLE;
			} else {
				return Decision.STAND;
			}
		}
		else if (directive.equalsIgnoreCase("S")) {
			return Decision.STAND;
		}
		else {
			return Decision.STAND;
		}		
			
	}
}
