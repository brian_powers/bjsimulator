/**
 * 
 */
package com.guyriley.bjsimulator.game.strategy;

import com.guyriley.bjsimulator.game.Card.Rank;
import com.guyriley.bjsimulator.game.Hand;
import com.guyriley.bjsimulator.game.Shoe;

/**
 * @author Brian S. Powers
 *
 */
public class DealerStrategy implements Strategy {

	/* (non-Javadoc)
	 * @see com.guyriley.bjsimulator.game.strategy.Strategy#getDecision(com.guyriley.bjsimulator.game.Hand, com.guyriley.bjsimulator.core.Rules, com.guyriley.bjsimulator.game.Shoe)
	 */
	@Override
	public Decision getDecision(Hand hand, Rank dealerUpcardRank, Shoe shoe) {
		
		// Simple dealer strategy, eventually refactor for changing rules
		if (hand.getValue() < 17) {
			return Decision.HIT;
		} else if (hand.getValue() == 17 && hand.isSoft()) {
			return Decision.HIT;
		} else {
			return Decision.STAND;
		}
	}

}
