/**
 * 
 */
package com.guyriley.bjsimulator.game.strategy;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.guyriley.bjsimulator.game.Card;
import com.guyriley.bjsimulator.game.Hand;

/**
 * @author Brian S. Powers
 *
 */
public class DealerStrategyTest {

	/**
	 * Test method for {@link com.guyriley.bjsimulator.game.strategy.DealerStrategy#getDecision(com.guyriley.bjsimulator.game.Hand, com.guyriley.bjsimulator.core.Rules, com.guyriley.bjsimulator.game.Shoe)}.
	 */
	@Test
	public void testGetDecision() {
		Strategy strat = new DealerStrategy();
		
		Hand hand = new Hand();
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(Card.Suit.HEARTS,Card.Rank.TWO));
		cards.add(new Card(Card.Suit.HEARTS,Card.Rank.SEVEN));
		cards.add(new Card(Card.Suit.HEARTS,Card.Rank.THREE));
		cards.add(new Card(Card.Suit.HEARTS,Card.Rank.FIVE));
		
		for (Card currentCard : cards) {
			hand.addCard(currentCard);
		}

		assertEquals(Strategy.Decision.STAND,strat.getDecision(hand, null, null));
	}

}
