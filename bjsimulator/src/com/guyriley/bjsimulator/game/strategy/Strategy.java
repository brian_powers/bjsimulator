/**
 * 
 */
package com.guyriley.bjsimulator.game.strategy;

import com.guyriley.bjsimulator.game.Card.Rank;
import com.guyriley.bjsimulator.game.Hand;
import com.guyriley.bjsimulator.game.Shoe;

/**
 * @author Brian S. Powers
 *
 */
public interface Strategy {

	public enum Decision {
		HIT, STAND, SPLIT, DOUBLE, SURRENDER

	}
	
	Decision getDecision(Hand hand, Rank upCardRank, Shoe shoe);
}
