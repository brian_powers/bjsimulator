/**
 * 
 */
package com.guyriley.bjsimulator;

import com.guyriley.bjsimulator.game.Hand;
import com.guyriley.bjsimulator.game.Shoe;
import com.guyriley.bjsimulator.game.strategy.Strategy;
import com.guyriley.bjsimulator.game.strategy.Strategy.Decision;
import com.guyriley.bjsimulator.game.strategy.DealerStrategy;

/**
 * @author Brian S. Powers
 *
 */
public class BJSimulator {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Shoe shoe = new Shoe(6);
		Strategy strategy = new DealerStrategy();
		int hands = 0;
		int underTwentyOne = 0;
		int busts = 0;
		
		while (shoe.getRemainingPercent() > 10) {
			Hand dealerHand = new Hand();
			dealerHand.addCard(shoe.dealCard());
			dealerHand.addCard(shoe.dealCard());
			
			while (strategy.getDecision(dealerHand, dealerHand.getCard(0).getRank(), shoe) != Decision.STAND) {
				dealerHand.addCard(shoe.dealCard());
			}
			hands++;
			if (dealerHand.getValue() > 21) {
				busts++;
			} else {
				underTwentyOne++;
			}
			System.out.println("Dealer's final hand: " + dealerHand.getValue());
		}
		System.out.println("Total hands: " + hands + " Busts: " + busts);
	
	}

}

